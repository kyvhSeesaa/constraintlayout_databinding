package vn.seesaa.databindingtest.utils;

import java.util.Random;

public class RandomMath {

    public static int generateRandomNumber(int min, int max){
        if(min > max) return -1;
        Random random = new Random();
        return random.nextInt(max + 1 - min) + min;// [min, max]
    }
}
