package vn.seesaa.databindingtest;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import vn.seesaa.databindingtest.databinding.ActivityMainBinding;
import vn.seesaa.databindingtest.model.PlaceWeather;
import vn.seesaa.databindingtest.utils.RandomMath;

public class MainActivity extends AppCompatActivity {
    private List<PlaceWeather> mPlaceWeatherList;
    private ActivityMainBinding mBinding;
    private PlaceWeather mPlaceWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        createData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    private void createData() {
        mPlaceWeatherList = new ArrayList<>();
        mPlaceWeatherList.add(new PlaceWeather("Hanoi", 36, "Rainy"));
        mPlaceWeatherList.add(new PlaceWeather("Tp.HCM", 42, "Sunny"));
        mPlaceWeatherList.add(new PlaceWeather("Moscow", 18, "Snowy"));
        mPlaceWeatherList.add(new PlaceWeather("New York", 25, "Cooly"));
        mBinding.setMainActivity(this);
        setWeatherOfRandomPlace();
        //mBinding.setPlaceWeather(mPlaceWeather);
    }

    public void setWeatherOfRandomPlace() {
        int randomNum = RandomMath.generateRandomNumber(0, mPlaceWeatherList.size() - 1);
        // mPlaceWeather = mPlaceWeatherList.get(randomNum);
        mBinding.setPlaceWeather(mPlaceWeatherList.get(randomNum));
    }
}
