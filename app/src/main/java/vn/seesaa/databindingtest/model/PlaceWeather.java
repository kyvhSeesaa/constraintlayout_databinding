package vn.seesaa.databindingtest.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import vn.seesaa.databindingtest.BR;

public class PlaceWeather extends BaseObservable {
    private int id;
    private String place;
    private int celsius;
    private String weatherType;

    public PlaceWeather() {}

    public PlaceWeather(String place, int celsius, String weatherType) {
        this.place = place;
        this.celsius = celsius;
        this.weatherType = weatherType;
    }

    @Bindable
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
        notifyPropertyChanged(BR.place);
    }

    @Bindable
    public int getCelsius() {
        return celsius;
    }

    public void setCelsius(int celsius) {
        this.celsius = celsius;
        notifyPropertyChanged(BR.celsius);
    }

    @Bindable
    public String getWeatherType() {
        return weatherType;
    }

    public void setWeatherType(String weatherType) {
        this.weatherType = weatherType;
        notifyPropertyChanged(BR.weatherType);
    }
}
